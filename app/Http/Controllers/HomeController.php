<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function export(Request $request)
    {
        if( !$file = $request->file('export'))
            return redirect()->route('home');
        $filepath = $file->store('xlsx');
        $reader = new Xlsx();
        $spreadsheet = $reader->load(storage_path('app/'.$filepath));
        $worksheet = $spreadsheet->getActiveSheet();
        if(Storage::has('export.zip'))
            Storage::delete('export.zip');
        $zip = new \ZipArchive();
        if($zip->open(storage_path('app/export.zip'), \ZipArchive::CREATE) !== TRUE)
            return redirect()->back()->with('error','Zip não pode ser criado');

        foreach ($worksheet->getRowIterator() as $index => $row) {
            if($index == 1)
                continue;
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);
            $filename = $cellIterator->current()->getValue() . '.txt';
            $cellIterator->next();
            $content = $cellIterator->current()->getValue();
            if(Storage::put(date('Y-m-d'). '/' . $filename, $content)){
                $zip->addFile(storage_path('app/' . date('Y-m-d'). '/' . $filename), $filename);
            }
        }
        $zip->close();
        Storage::deleteDirectory('xlsx/');
        Storage::deleteDirectory(date('Y-m-d').'/');
        return response()->download(storage_path('app/export.zip'));
    }

    public function download($filename)
    {
        return response()->download(storage_path('app/' . $filename));
    }
}
