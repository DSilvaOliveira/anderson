@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nome do arquivo</th>
                                    <th scope="col">Link download</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($filereturn as $index => $file)
                                <tr>
                                    <th scope="row">{{$index}}</th>
                                    <td>{{$file}}</td>
                                    <td><a class="btn btn-link" href="{{route('download', ['filename' => $file])}}" target="_blank">Download</a></td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
